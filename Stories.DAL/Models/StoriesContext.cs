using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Stories.DAL.Models.Mapping;

namespace Stories.DAL.Models
{
    public partial class StoriesContext : DbContext
    {
        static StoriesContext()
        {
            Database.SetInitializer<StoriesContext>(null);
        }

        public StoriesContext()
            : base("Name=StoriesContext")
        {
        }

        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<StoryGroup> StoryGroups { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<viewGroup> viewGroups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AspNetRoleMap());
            modelBuilder.Configurations.Add(new AspNetUserClaimMap());
            modelBuilder.Configurations.Add(new AspNetUserLoginMap());
            modelBuilder.Configurations.Add(new AspNetUserMap());
            modelBuilder.Configurations.Add(new GroupMap());
            modelBuilder.Configurations.Add(new LogMap());
            modelBuilder.Configurations.Add(new StoryMap());
            modelBuilder.Configurations.Add(new StoryGroupMap());
            modelBuilder.Configurations.Add(new UserGroupMap());
            modelBuilder.Configurations.Add(new viewGroupMap());
        }
    }
}
