﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserStories.Models
{
    public class JsonStoryModel
    {
        public string HTMLString { get; set; }
        public bool NoMoreData { get; set; }
    }
}