﻿using System;
using System.Reflection;
using Stories.BLL.Models;
using Stories.DAL.Models;
using UserStories.Logging;

namespace Stories.BLL
{
    public class ModelAdapter
    {
        public static StoryBM StoryAdaptation(Story entity)
        {
            try
            {
                var model = new StoryBM()
                {
                    Id = entity.Id,
                    Content = entity.Content,
                    Description = entity.Description,
                    PostedOn = entity.PostedOn,
                    StoryGroupId = entity.StoryGroupId,
                    Title = entity.Title,
                    UserId = entity.UserId
                };

                if (entity.AspNetUser != null)
                {
                    model.User = new UserBM()
                    {
                        Email = entity.AspNetUser.Email,
                        Id = entity.AspNetUser.Id,
                        UserName = entity.AspNetUser.UserName
                    };

                }

                return model;

            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }
    }
}
