﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stories.BLL.Models;

namespace Stories.BLL
{
    public interface IBusinessManager
    {
        List<StoryBM> GetAllStories(int blockNumber, int blockSize);
        StoryBM GetStory(int id);

        List<GroupBM> GetAllGroups();

        List<GroupBM> GetViewGroups();

        void AddStory(StoryBM model);

        void AddGroup(GroupBM model);
    }
}
