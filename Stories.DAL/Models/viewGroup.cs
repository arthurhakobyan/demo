using System;
using System.Collections.Generic;

namespace Stories.DAL.Models
{
    public partial class viewGroup
    {
        public long RowId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> StoryCount { get; set; }
        public Nullable<int> UserCount { get; set; }
    }
}
