using System;
using System.Collections.Generic;

namespace Stories.DAL.Models
{
    public partial class StoryGroup
    {
        public int Id { get; set; }
        public int StoryId { get; set; }
        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
        public virtual Story Story { get; set; }
    }
}
