﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Stories.BLL;
using Stories.DAL;

namespace UserStories.Controllers
{
    public class BaseController : Controller
    {
        protected IBusinessManager _BusinessManager;

        public BaseController()
        {
            _BusinessManager = new BusinessManager(new DataManager());
        }

        public BaseController(IBusinessManager businessManager)
        {
            _BusinessManager = businessManager;
        }

        /// <summary>
        /// Get current user Id
        /// </summary>
        public string UserId
        {
            get
            {
                return System.Web.HttpContext.Current.User.Identity.GetUserId();
            }
        }
    }
}