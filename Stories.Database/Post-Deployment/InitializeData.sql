﻿USE [Stories]
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', NULL, 0, N'AHV7LiBtyt7G38Ps8yQDTf24sE0rZhO7X7lSGM0A8MikHI36HjUKWNCmOQPKo3OFQw==', N'443588a8-c50d-453e-a290-7ef1409737aa', NULL, 0, 0, NULL, 0, 0, N'user')
GO
SET IDENTITY_INSERT [dbo].[Story] ON 

GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (1, N'story 1', N'first story', N'bla bla bla', CAST(N'2015-08-13 02:25:42.963' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (2, N'story 2', N'second story', N'bla bla bla', CAST(N'2015-08-13 02:55:02.117' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (3, N'story 3', N'3', N'bla bla bla', CAST(N'2015-08-13 02:58:27.890' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (4, N'story 4', N'sad', N'asdasd', CAST(N'2015-08-13 03:52:03.600' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (5, N'story 5', N'asda', N'asdasd', CAST(N'2015-08-13 03:52:19.730' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (6, N'story 6', N'asdasd', N'asdasda', CAST(N'2015-08-13 03:52:28.003' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (7, N'story 7', N'sdfsdf', N'sdfsdf', CAST(N'2015-08-13 03:52:38.073' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (8, N'story 8', N'test', N'asd', CAST(N'2015-08-13 03:52:53.447' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (9, N'story 9', N'qweqwe', N'qweqwe', CAST(N'2015-08-13 03:53:01.873' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (10, N'story 10 ', N'asd sa ', N'asd asd asd as', CAST(N'2015-08-13 03:53:11.313' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (11, N'story 11', N'asd a', N'asd as', CAST(N'2015-08-13 03:53:27.220' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (12, N'story 12', N'asd asd ', N'asdasd ', CAST(N'2015-08-13 03:53:33.673' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (13, N'story 13', N'asd asd ', N'asd asd as', CAST(N'2015-08-13 03:53:39.563' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (14, N'story 14', N'asd asd ', N'asd asd sa', CAST(N'2015-08-13 03:53:45.453' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (15, N'story 15', N'asd asd ', N'asd asd asd asd sad asd', CAST(N'2015-08-13 03:53:52.850' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (16, N'story 16', N'asdasd sad as as dsa', N'asd asd asdasdasdas', CAST(N'2015-08-13 03:54:01.277' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (17, N'story 17', N'asdasdasdasdasd', N'asdasdasdasd', CAST(N'2015-08-13 03:54:10.137' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (18, N'story 18', N'asd asd', N'as dasd', CAST(N'2015-08-13 03:54:18.277' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (19, N'story 19', N'sd fsd fsd', N'sdfsdfsdfsdf', CAST(N'2015-08-13 03:54:27.953' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (20, N'story 20', N'dsfsdfsdfsd', N'sdfsdfsdfsdfsdfsdf', CAST(N'2015-08-13 03:54:39.543' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (21, N'story 21', N'sdf sdf ', N'sdf sdf ', CAST(N'2015-08-13 03:54:50.393' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 1)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (22, N'story 22', N'sdf sdf ', N'sd fsdffsd', CAST(N'2015-08-13 03:55:02.217' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (23, N'story 23', N'sdffsdf ', N'sd fsdfsdf', CAST(N'2015-08-13 03:55:13.963' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (24, N'story 24', N'sdf sdfsd', N's df sd fsdfsdfsdfsdf', CAST(N'2015-08-13 03:55:25.110' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (25, N'story 25', N'sdfsdfsdfsdf', N' sdfsdfsdf sdf', CAST(N'2015-08-13 03:55:33.487' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (26, N'story 26', N'sdfsdfsdf', N'sdfsdfsdfsdf sdf sdf dsf', CAST(N'2015-08-13 03:55:45.317' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (27, N'story 27', N'sfsdfsdsdfsdfsdfs', N'dsdsdfsdfsdfsdf', CAST(N'2015-08-13 03:55:56.677' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (28, N'story 28', N'sdfsdfsdfsdf', N'sdfsdf sd  fsd sdf sd f', CAST(N'2015-08-13 03:56:06.627' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[Story] ([Id], [Title], [Description], [Content], [PostedOn], [UserId], [StoryGroupId]) VALUES (29, N'story 29', N' sfsfsdf', N'fsdfsdfsdfsdf', CAST(N'2015-08-13 03:56:24.513' AS DateTime), N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
SET IDENTITY_INSERT [dbo].[Story] OFF
GO
SET IDENTITY_INSERT [dbo].[Group] ON 

GO
INSERT [dbo].[Group] ([Id], [Name], [Description]) VALUES (1, N'group 1', N'first group ')
GO
INSERT [dbo].[Group] ([Id], [Name], [Description]) VALUES (2, N'group 2', N'second group ')
GO
INSERT [dbo].[Group] ([Id], [Name], [Description]) VALUES (3, N'Group 3', N'Third group')
GO
INSERT [dbo].[Group] ([Id], [Name], [Description]) VALUES (4, N'Group 4', N'group 4')
GO
SET IDENTITY_INSERT [dbo].[Group] OFF
GO
SET IDENTITY_INSERT [dbo].[UserGroup] ON 

GO
INSERT [dbo].[UserGroup] ([Id], [UserId], [GroupId]) VALUES (2, N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 2)
GO
INSERT [dbo].[UserGroup] ([Id], [UserId], [GroupId]) VALUES (3, N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 3)
GO
INSERT [dbo].[UserGroup] ([Id], [UserId], [GroupId]) VALUES (4, N'1024e3be-ab32-49f2-84a5-9b925e29eb3c', 4)
GO
SET IDENTITY_INSERT [dbo].[UserGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[StoryGroup] ON 

GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (1, 1, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (2, 2, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (3, 3, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (4, 4, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (5, 5, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (6, 6, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (7, 7, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (8, 8, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (9, 9, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (10, 10, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (11, 11, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (12, 12, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (13, 13, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (14, 14, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (15, 15, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (16, 16, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (17, 17, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (18, 18, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (19, 19, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (20, 20, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (21, 21, 1)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (22, 22, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (23, 23, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (24, 24, 2)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (25, 25, 4)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (26, 26, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (27, 27, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (28, 28, 3)
GO
INSERT [dbo].[StoryGroup] ([Id], [StoryId], [GroupId]) VALUES (29, 29, 4)
GO
SET IDENTITY_INSERT [dbo].[StoryGroup] OFF
GO
