﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Stories.BLL.Models;
using UserStories.Logging;
using UserStories.Models;

namespace UserStories
{
    public class ModelAdapter
    {
        public static StoryModel StoryAdaptation(StoryBM modelBM)
        {
            try
            {
                var model = new StoryModel()
                {
                    Id = modelBM.Id,
                    Content = modelBM.Content,
                    Description = modelBM.Description,
                    PostedOn = modelBM.PostedOn,
                    Title = modelBM.Title,
                    GroupIds = new int[] { modelBM.StoryGroupId }
                };

                return model;

            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }
    }
}