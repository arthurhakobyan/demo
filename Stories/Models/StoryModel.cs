﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserStories.Models
{
    public class StoryModel
    {

        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.")]        
        [Display(Name = "Title")]
        public string Title { get; set; }

        [StringLength(512, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [StringLength(512, ErrorMessage = "The {0} must be at least {2} characters long.")]
        [Display(Name = "Content")]
        public string Content { get; set; }
        public DateTime PostedOn { get; set; }

        [Required]
        public int[] GroupIds { get; set; }



    }
}