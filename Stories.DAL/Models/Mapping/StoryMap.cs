using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Stories.DAL.Models.Mapping
{
    public class StoryMap : EntityTypeConfiguration<Story>
    {
        public StoryMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(512);

            this.Property(t => t.Content)
                .HasMaxLength(512);

            this.Property(t => t.UserId)
                .IsRequired()
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("Story");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.PostedOn).HasColumnName("PostedOn");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.StoryGroupId).HasColumnName("StoryGroupId");

            // Relationships
            this.HasRequired(t => t.AspNetUser)
                .WithMany(t => t.Stories)
                .HasForeignKey(d => d.UserId);

        }
    }
}
