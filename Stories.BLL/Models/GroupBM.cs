﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stories.BLL.Models
{
    public class GroupBM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StoryCount { get; set; }
        public int UserCount { get; set; }
        public string UserId { get; set; }

    }
}
