using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Stories.DAL.Models.Mapping
{
    public class viewGroupMap : EntityTypeConfiguration<viewGroup>
    {
        public viewGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.RowId);

            // Properties
            this.Property(t => t.RowId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(512);

            // Table & Column Mappings
            this.ToTable("viewGroups");
            this.Property(t => t.RowId).HasColumnName("RowId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.StoryCount).HasColumnName("StoryCount");
            this.Property(t => t.UserCount).HasColumnName("UserCount");
        }
    }
}
