Platform: ASP.net MVC, .Net framework 4.5.1, EntityFramework 6.0, SQL server 2012, 

- for running application need to do following steps
1. creating database with initial data - go to DataLayer\Stories.database project
2. double click to Stories.Database.publish.xml
3. configure target database connection and publish database
4. copy connection string and replace it into connection string values of Web.Config StoriesEntities and DefaultConnection in Stories project
5. run application
6. register user 