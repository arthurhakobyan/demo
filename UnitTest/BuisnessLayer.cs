﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stories.BLL;
using Stories.DAL;

namespace UnitTest
{
    [TestClass]
    public class BuisnessLayer
    {
        [TestMethod]
        public void ValidateGroups()
        {
            IBusinessManager bm = new BusinessManager(new DataManager());

            var groups = bm.GetAllGroups();

            Assert.IsNotNull(groups);
        }
    }
}
