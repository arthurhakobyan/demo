﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Stories.BLL;
using Stories.BLL.Models;
using UserStories.JQueryDataTablesMvc;
using UserStories.Logging;
using UserStories.Models;

namespace UserStories.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
       

        public ActionResult Index()
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start HomeController.Index()");

            return View();
        }


        public ActionResult Groups()
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start HomeController.Groups()");

            return View();
        }

        

        [HttpPost]
        public JsonResult GetGroups(GroupModel model)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start HomeController.GetGroups()");
            var groups =_BusinessManager.GetViewGroups();

            return Json(new JQueryDataTablesResponse<GroupBM>(items: groups,
                totalRecords: groups.Count,
                totalDisplayRecords: groups.Count,
                sEcho: model.sEcho));
        }

        [HttpPost]
        public ActionResult Groups(GroupModel model)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start HomeController.Groups() POST");
            if (ModelState.IsValid)
            {
               _BusinessManager.AddGroup(new GroupBM()
                {
                    Description = model.Description,
                    Name = model.Name,
                    UserId = UserId
                });
            }

            return View("Index");
        }




    }
}