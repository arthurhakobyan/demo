﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Stories.DAL.Models;
using UserStories.Logging;

namespace Stories.DAL
{
    public class DataManager : IDataManager
    {
        #region methods
        /// <summary>
        /// Returns one block of story items
        /// </summary>
        /// <param name="blockNumber">Starting from 1</param>
        /// <param name="BlockSize">Items count in a block</param>
        /// <returns></returns>
        List<Story> IDataManager.GetAllStories(int blockNumber, int blockSize)
        {
            try
            {
                int startIndex = (blockNumber - 1) * blockSize;

                using (var dbContext = new StoriesContext())
                {
                    return dbContext.Stories.Include("AspNetUser").OrderByDescending(s => s.PostedOn).Skip(startIndex).Take(blockSize).ToList();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        Story IDataManager.GetStory(int id)
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    return dbContext.Stories
                            .Include("AspNetUser")
                            .Include("StoryGroups")
                            .Where(s => s.Id == id).FirstOrDefault();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

         List<Group> IDataManager.GetAllGroups()
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    return dbContext.Groups.ToList();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

         List<viewGroup> IDataManager.GetViewGroups()
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    return dbContext.viewGroups.ToList();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

         int IDataManager.AddStory(Story entity)
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    dbContext.Stories.Add(entity);
                    dbContext.SaveChanges();
                }

                return entity.Id;
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

         void IDataManager.AddStoryGroup(StoryGroup entity)
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    dbContext.StoryGroups.Add(entity);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        int IDataManager.AddGroup(Group entity)
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    dbContext.Groups.Add(entity);
                    dbContext.SaveChanges();
                }

                return entity.Id;
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        void IDataManager.AddUserGroup(UserGroup entity)
        {
            try
            {
                using (var dbContext = new StoriesContext())
                {
                    dbContext.UserGroups.Add(entity);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        #endregion methods
    }

}
