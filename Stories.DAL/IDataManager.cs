﻿using System.Collections.Generic;
using Stories.DAL.Models;

namespace Stories.DAL
{
    public interface IDataManager
    {
        List<Story> GetAllStories(int blockNumber, int blockSize);
        Story GetStory(int id);
        List<Group> GetAllGroups();
        List<viewGroup> GetViewGroups();
        int AddStory(Story entity);
        void AddStoryGroup(StoryGroup entity);
        int AddGroup(Group entity);
        void AddUserGroup(UserGroup entity);
    }
}
