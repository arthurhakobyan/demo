using System;
using System.Collections.Generic;

namespace Stories.DAL.Models
{
    public partial class Story
    {
        public Story()
        {
            this.StoryGroups = new List<StoryGroup>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public System.DateTime PostedOn { get; set; }
        public string UserId { get; set; }
        public int StoryGroupId { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ICollection<StoryGroup> StoryGroups { get; set; }
    }
}
