﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace UserStories.Logging
{
    
    /// <summary>
    /// Helper class, wrapped the log4net third party application
    /// please, see http://log2console.codeplex.com/
    /// for more information
    /// </summary>

        public class LogHelper
    {

        static string GetLoggerName(MethodBase methodInfo, string loggerName)
        {
            if (String.IsNullOrEmpty(loggerName))
            {
                return String.Format("{0}.{1}->{2}", methodInfo.DeclaringType.FullName, methodInfo.Name, methodInfo.Module.Name);
            }
            else
                return loggerName;
        }


        #region Constants

        #endregion Constants

        #region Constructor

        static LogHelper()
        {
            log4net.Config.XmlConfigurator.Configure();
        }


        #endregion Constructor


        #region Debug
        static public void WriteDebug(MethodBase methodInfo, string msg, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Debug(msg);
        }

        static public void WriteDebugFormatted(MethodBase methodInfo, string msg, string loggerName = "", params object[] args)
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.DebugFormat(msg, args);
        }

        static public void WriteDebug(MethodBase methodInfo, Exception exp, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Debug(exp.Message, exp);
        }

        #endregion Debug

        #region Error
        static public void WriteError(MethodBase methodInfo, string msg, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Error(msg);
        }

        static public void WriteErrorFormatted(MethodBase methodInfo, string msg, string loggerName = "", params object[] args)
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.ErrorFormat(msg, args);
        }

        static public void WriteError(MethodBase methodInfo, Exception exp, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Error(exp.Message, exp);
        }

        #endregion Error

        #region Info
        static public void WriteInfo(MethodBase methodInfo, string msg, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Info(msg);
        }

        static public void WriteInfoFormatted(MethodBase methodInfo, string msg, string loggerName = "", params object[] args)
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.InfoFormat(msg, args);
        }


        static public void WriteInfo(MethodBase methodInfo, Exception exp, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Info(exp.Message, exp);
        }

        #endregion Info

        #region Fatal
        static public void WriteFatal(MethodBase methodInfo, string msg, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Fatal(msg);
        }

        static public void WriteFatalFormatted(MethodBase methodInfo, string msg, string loggerName = "", params object[] args)
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.FatalFormat(msg, args);
        }

        static public void WriteFatal(MethodBase methodInfo, Exception exp, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Fatal(exp.Message, exp);
        }

        #endregion Fatal

        #region Warn
        static public void WriteWarning(MethodBase methodInfo, string msg, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Warn(msg);
        }

        static public void WriteWarningFormatted(MethodBase methodInfo, string msg, string loggerName = "", params object[] args)
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.WarnFormat(msg, args);
        }

        static public void WriteWarning(MethodBase methodInfo, Exception exp, string loggerName = "")
        {
            // Create a logger.
            ILog _log = LogManager.GetLogger(GetLoggerName(methodInfo, loggerName));
            _log.Warn(exp.Message, exp);
        }

        #endregion Warn




    }
}
