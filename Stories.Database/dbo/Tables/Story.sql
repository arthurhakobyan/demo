﻿CREATE TABLE [dbo].[Story] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Title]        NVARCHAR (50)  NOT NULL,
    [Description]  NVARCHAR (512) NULL,
    [Content]      NVARCHAR (512) NULL,
    [PostedOn]     DATETIME       CONSTRAINT [DF_Story_PostedOn] DEFAULT (getdate()) NOT NULL,
    [UserId]       NVARCHAR (128) NOT NULL,
    [StoryGroupId] INT            NOT NULL,
    CONSTRAINT [PK_Story] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Story_AspNetUsers] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);

