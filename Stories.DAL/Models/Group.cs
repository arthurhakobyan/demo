using System;
using System.Collections.Generic;

namespace Stories.DAL.Models
{
    public partial class Group
    {
        public Group()
        {
            this.StoryGroups = new List<StoryGroup>();
            this.UserGroups = new List<UserGroup>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<StoryGroup> StoryGroups { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}
