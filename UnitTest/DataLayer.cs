﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stories.DAL;

namespace UnitTest
{
    [TestClass]
    public class DataLayer
    {
        [TestMethod]
        public void ValidateGroups()
        {
            IDataManager dm = new DataManager();

            var groups = dm.GetAllGroups();

            Assert.IsNotNull(groups);
        }
    }
}
