﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserStories.Logging;
using UserStories.Models;
using System.Reflection;
using Stories.BLL.Models;
using System.IO;

namespace UserStories.Controllers
{

    [Authorize]
    public class StoriesController : BaseController
    {
        int BlockSize = 25;
        public ActionResult Index()
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.Index()");

            var stories = _BusinessManager.GetAllStories(1, BlockSize);

            return View(stories);
        }

        public ActionResult Details(int id)
        {
            var model = _BusinessManager.GetStory(id);

            var group = _BusinessManager.GetAllGroups().Where(g => g.Id == model.StoryGroupId).FirstOrDefault();

            if (group != null)
            {
                ViewData["GroupName"] = group.Name;
            }

            return View(ModelAdapter.StoryAdaptation(model));
        }

        public ActionResult Edit(int id)
        {
            var model = _BusinessManager.GetStory(id);

            ViewData["Groups"] = _BusinessManager.GetAllGroups();

            return View(ModelAdapter.StoryAdaptation(model));
        }

        [HttpPost]
        public ActionResult Edit(StoryModel model)
        {
            return RedirectToAction("Index");
        }
            

        public ActionResult NewStory()
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.NewStory()");

            ViewData["Groups"] = _BusinessManager.GetAllGroups();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewStory(StoryModel model)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.NewStory()");

            ViewData["Groups"] = _BusinessManager.GetAllGroups();

            if (ModelState.IsValid)
            {
                foreach (var groupId in model.GroupIds)
                {
                    var story = new StoryBM()
                    {
                        UserId = UserId,
                        Title = model.Title,
                        Description = model.Description,
                        Content = model.Content,
                        PostedOn = DateTime.Now,
                        StoryGroupId = groupId
                    };

                    _BusinessManager.AddStory(story);
                }

                return RedirectToAction("Index", new { succeed = true });
            }

            return View(model);
        }


        public ActionResult StoryList(List<StoryBM> Model)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.NewStory()");
            return PartialView(Model);
        }

        [HttpPost]
        public ActionResult InfinateScroll(int BlockNumber)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.InfinateScroll()");

            var stories = _BusinessManager.GetAllStories(BlockNumber, BlockSize);

            JsonStoryModel jsonModel = new JsonStoryModel();
            jsonModel.NoMoreData = stories.Count < BlockSize;
            jsonModel.HTMLString = RenderPartialViewToString("StoryList", stories);

            return Json(jsonModel);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            LogHelper.WriteDebug(MethodBase.GetCurrentMethod(), "Start StoriesController.RenderPartialViewToString()");

            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

    }
}