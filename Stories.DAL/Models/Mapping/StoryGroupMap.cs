using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Stories.DAL.Models.Mapping
{
    public class StoryGroupMap : EntityTypeConfiguration<StoryGroup>
    {
        public StoryGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("StoryGroup");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.StoryId).HasColumnName("StoryId");
            this.Property(t => t.GroupId).HasColumnName("GroupId");

            // Relationships
            this.HasRequired(t => t.Group)
                .WithMany(t => t.StoryGroups)
                .HasForeignKey(d => d.GroupId);
            this.HasRequired(t => t.Story)
                .WithMany(t => t.StoryGroups)
                .HasForeignKey(d => d.StoryId);

        }
    }
}
