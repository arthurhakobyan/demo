﻿CREATE TABLE [dbo].[StoryGroup] (
    [Id]      INT IDENTITY (1, 1) NOT NULL,
    [StoryId] INT NOT NULL,
    [GroupId] INT NOT NULL,
    CONSTRAINT [PK_StoryGroup] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StoryGroup_Group] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Group] ([Id]),
    CONSTRAINT [FK_StoryGroup_Story] FOREIGN KEY ([StoryId]) REFERENCES [dbo].[Story] ([Id])
);

