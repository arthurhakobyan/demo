﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stories.BLL.Models
{
    public class StoryBM
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public DateTime PostedOn { get; set; }
        public string UserId { get; set; }
        public int StoryGroupId { get; set; }
        public UserBM User { get; set; }
    }
}
