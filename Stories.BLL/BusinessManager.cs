﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Stories.BLL.Models;
using Stories.DAL;
using Stories.DAL.Models;
using UserStories.Logging;

namespace Stories.BLL
{
    public class BusinessManager : IBusinessManager
    {
        #region members

        private IDataManager _DataManager;

        #endregion members

        #region constructors

        public BusinessManager(IDataManager dataManager)
        {
            _DataManager = dataManager;
        }

        #endregion constructors

        #region methods
        StoryBM IBusinessManager.GetStory(int id)
        {
            try
            {
                var storyEntity = _DataManager.GetStory(id);

                return ModelAdapter.StoryAdaptation(storyEntity);
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }

        }

        List<StoryBM> IBusinessManager.GetAllStories(int blockNumber, int blockSize)
        {
            try
            {
                var storyEntities = _DataManager.GetAllStories(blockNumber, blockSize);

                var model = storyEntities.Select(s => ModelAdapter.StoryAdaptation(s));

                return model.ToList();
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        List<GroupBM> IBusinessManager.GetAllGroups()
        {
            try
            {
                var groupEntities = _DataManager.GetAllGroups();

                var model = groupEntities.Select(s => new GroupBM()
                {
                    Id = s.Id,
                    Description = s.Description,
                    Name = s.Name
                });

                return model.ToList();
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        List<GroupBM> IBusinessManager.GetViewGroups()
        {
            try
            {
                var groupEntities = _DataManager.GetViewGroups();

                var model = groupEntities.Select(g => new GroupBM()
                {
                    Id = g.RowId,
                    Description = g.Description,
                    Name = g.Name,
                    StoryCount = g.StoryCount ?? 0,
                    UserCount = g.UserCount ?? 0
                });

                return model.ToList();
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        void IBusinessManager.AddStory(StoryBM model)
        {
            try
            {
                var storyId = _DataManager.AddStory(new Story()
                {
                    Title = model.Title,
                    Content = model.Content,
                    Description = model.Description,
                    PostedOn = DateTime.Now,
                    UserId = model.UserId,
                    StoryGroupId = model.StoryGroupId
                });

                _DataManager.AddStoryGroup(new StoryGroup()
                {
                    StoryId = storyId,
                    GroupId = model.StoryGroupId
                });
            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        void IBusinessManager.AddGroup(GroupBM model)
        {
            try
            {
                var groupId = _DataManager.AddGroup(new Group()
                {
                    Name = model.Name,
                    Description = model.Description
                });


                _DataManager.AddUserGroup(new UserGroup()
                {
                    UserId = model.UserId,
                    GroupId = groupId
                });

            }
            catch (Exception exp)
            {
                LogHelper.WriteError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        #endregion methods
    }
}
