﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Stories.BLL.Models;
using UserStories.JQueryDataTablesMvc;

namespace UserStories.Models
{
    public class GroupModel : JQueryDataTablesModel
    {
        public JQueryDataTablesResponse<GroupBM> DataTable { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.")]        
        public string Name { get; set; }

        public string Description { get; set; }
    }
}